<?php

use coldApe\extend;
use coldApe\upload;
use coldApe\uploadException;

require_once 'vendor/autoload.php';

try {
    // 两种方式实例化
    // $upload = new upload('image');
    $upload = new upload($_FILES['video']);

    // 如果不需要检测文件是否存在 需要直接覆盖则修改为false
    $upload->check_file_exists = false;

    // 设置存储文件路径
    $upload->setPath(__DIR__ . '/upload');

    // 设置限制文件大小 (支持单位:B,K,M,G)
    $upload->setMaxSize('20G');

    // 设置文件名  [uniqueName方法生成随机文件名]
    $upload->setName($upload->uniqueName());

    // 设置限制扩展名类型 可以为字符串也可为数组
    $upload->setExtensionLimit(['jpg', 'mp4', 'mp3']);

    // $upload->setExtension(['png', 'jpg']);

    // 设置限制的MIME类型 同上 可为字符串亦可为数组
     $upload->setMimeLimit(['image/png', 'image/jpeg', 'video/mp4', 'audio/mpeg']);

    // 设置外部访问链接 默认域名+文件路径下
    // $upload->setPublicPath('upload');

    // 执行上传操作
    $up = $upload->save();

    $return = [
        '文件名' => $upload->getName(),
        '文件名+后缀' => $upload->getFullName(),
        '文件路径' => $upload->getPath(),
        '文件路径+文件名+后缀' => $upload->getFullPath(),
        '文件扩展名' => $upload->getExtension(),
        '文件MD5值' => $upload->getMd5(),
        '文件MIME' => $upload->getMime(),
        '文件大小' => $upload->getSize(),
        '外部访问链接' => $upload->getPublicPath(),
        '文件宽高和长度' => $upload->getFileInfo(),
    ];

    ps($return);
} catch (uploadException $e) {
    echo $e->getMessage();
}


// 对文件进一步操作 例如压缩 修改尺寸等
try {
    $extend = new extend($upload->getFullPath());

    // 如果不需要检测文件是否存在 需要直接覆盖则修改为false
    $extend->check_file_exists = false;

    // 设置文件后缀
//    $extend->setSuffix('_thumb');

    // 设置文件前缀
//    $extend->setPrefix('hello_');

    // 设置尺寸
//    $extend->setSize(100,100);

    // 修改扩展名 格式转换
    // $extend->setExtension('png');

    // 修改查看图片的web路径
    // $extend->setPublicPath('/askdjksal/sdjsad');

    // 设置其他独立的ffmpeg参数
    // $extend->setOtherCommand([
    //     '-r 30',
    //     '-loop 1'
    // ]);

    // 修改文件路径
     $extend->setPath(__DIR__.'/upload/');

    // 修改文件名
     $extend->setName('修改文件名');

    // 设置10个线程
    // $extend->setThreads(10);

    // 压缩视频码率 码率越低大小越小 但会越模糊
    // $extend->setVideoBit(1000);

    // 修改视频为H.264格式
    // $extend->setVideoH264();

    // 设置视频的渐入渐出 out渐出 in渐入
    // $extend->setVideoFade('out', 12.5, 0.5);

    // 设置图片为webp格式
    // $extend->setImageWebp()->getCommand();

    // 视频截图
//     $extend->setVideoShot(1, 'png');

    // 独立执行其他命令
    // $extend->setVideoH264()->setName('1');
    // $extend->setVideoH264()->setName('2');

    // 合并两个视频必须先转成同样的码率【这是个坑】可以用上面的命令先转
    // $extend->run('ffmpeg -y -f concat -safe 0 -i video.txt -c:a copy '.__DIR__.'/upload/视频合并.mp4');

    $extend->setExtension('mp4');

    $run = $extend->run();


    
    $return = [
        '文件名' => $extend->getName(),
        '文件名+后缀' => $extend->getFullName(),
        '文件路径' => $extend->getPath(),
        '文件路径+文件名+后缀' => $extend->getFullPath(),
        '文件扩展名' => $extend->getExtension(),
        '文件MD5值' => $extend->getMd5(),
        '文件MIME' => $extend->getMime(),
        '文件大小' => $extend->getSize(),
        '外部访问链接' => $extend->getPublicPath(),
        'FFMPEG执行语句' => $extend->getCommand(),
        '文件宽高和长度' => $extend->getFileInfo(),
    ];

    ps($return);
} catch (uploadException $e) {
    echo $e->getMessage();
}


function ps(...$content)
{
    echo '<pre>';

    var_dump($content);

    echo '</pre>';
}
