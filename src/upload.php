<?php
namespace coldApe;

class upload
{
    use command;

    /**
     * 初始化上传设置
     * @param array|string $file 上传字段名或者上传的文件
     * @throws uploadException
     */
    public function __construct($file)
    {
        if (is_array($file)) {
            $this->set($file);
        } else {
            if (!isset($_FILES[$file]) || empty($_FILES[$file])) {
                $this->setError('文件不存在');
            }

            $this->set($_FILES[$file]);
        }
    }

    /**
     * 保存文件
     *
     * @return bool
     * @throws uploadException
     */
    public function save()
    {
        // 上传前检测
        $this->checkExtension()->checkMime()->checkSize()->checkPath();

        // 检测文件是否存在
        if ($this->check_file_exists) {
            $this->checkExists();
        }

        // 上传文件
        $move = move_uploaded_file($this->tmp_path, $this->getFullPath());

        // 上传失败
        if (!$move) {
            $this->setError('上传失败');
        }

        $this->file_size = filesize($this->getFullPath());

        $this->tmp_path = $this->getFullPath();

        return true;
    }
}
