<?php

namespace coldApe;

class extend
{
    use command;

    // ffmpeg命令
    private $ffmpeg_command = [];

    // ffmpeg滤镜
    private $ffmpeg_filter = [];

    // 输出文件
    private $out_file = '';

/*-----------------------------------设置----------------------------------*/
    public function __construct($file)
    {
        $this->set($file);
    }

    /**
     * 设置宽高滤镜
     *
     * @param int $width  宽度
     * @param int $heigth 高度
     * @return $this
     */
    public function setSize($width, $heigth)
    {
        $this->ffmpeg_filter[] = "scale={$width}:{$heigth}";

        return $this;
    }

    /**
     * 设置线程数
     *
     * @param int $threads 线程数
     * @return $this
     */
    public function setThreads($threads)
    {
        $this->ffmpeg_command[] = "-threads {$threads}";

        return $this;
    }

    /**
     * 设置为webp的输出格式 压缩 该方法需要有ffmpeg的libwebp才可以使用
     *
     * @param int $is_damage 是否有损压缩 默认有损压缩 普通压缩无需填因为质量都是一样的 只有当使用了webp才有区别
     * 【png建议使用无损压缩 其他格式建议使用有损压缩 因无损压缩用的时长会更多且其他格式因图片质量高大小可能变大】
     * @return $this
     * @throws uploadException
     */
    public function setImageWebp($is_damage = 1)
    {
        $this->checkType('image');

        // 有损压缩
        $commond = '-quality 75';

        // 无损压缩
        if (!$is_damage) {
            $commond = '-lossless -quality 100';
        }

        $this->ffmpeg_command[] = "-codec libwebp ".$commond;

        $this->extension = 'webp';

        return $this;
    }

    /**
     * 设置视频码率【码率越低越模糊】
     *
     * @param int $bit 码率数值
     * @return extend
     * @throws uploadException
     */
    public function setVideoBit($bit)
    {
        $this->checkType('video');

        $this->ffmpeg_command[] = "-b:v {$bit}k";

        return $this;
    }

    /**
     * 渐进渐出滤镜
     *
     * @param string $inorout 渐入还是渐出 in=渐入 out=渐出
     * @param int $st 开始时长 开头则为0
     * @param int $d 渐进执行时长
     * @return $this
     * @throws uploadException
     */
    public function setVideoFade($inorout, $st, $d)
    {
        $this->checkType('video');

        $this->ffmpeg_filter[] = "fade=t={$inorout}:st={$st}:d={$d}";

        return $this;
    }

    /**
     * 设置编码为X264
     *
     * @return $this
     * @throws uploadException
     */
    public function setVideoH264()
    {
        $this->checkType('video');

        $this->ffmpeg_command[] = '-vcodec h264';

        return $this;
    }

    /**
     * 视频截图
     *
     * @param int $start 开始时间
     * @param string $type 类型 也是文件的后缀
     * @param int $time 需要截取多少秒【gif才需要】
     * @return $this
     * @throws uploadException
     */
    public function setVideoShot($start = 0, $type = 'jpg', $time = 1)
    {
        $this->checkType('video');

        switch ($type) {
            case 'gif':
                // 截图为gif
                $command = "-ss {$start} -t {$time} -f gif";
                break;
            default:
                // 截图为其他图片格式
                $command = "-ss {$start} -frames:v 1 -f image2";
        }

        // 设置命令
        $this->ffmpeg_command[] = $command;

        // 设置转换后的扩展名
        $this->extension = $type;

        return $this;
    }

    /**
     * 设置其他FFMPEG参数
     *
     * @param array|string $command 当该类没有提供适合的FFMPEG参数 可自定义FFMPEG参数执行
     * @return $this
     */
    public function setOtherCommand($command)
    {
        $this->ffmpeg_command = $command;

        if (!is_array($command)) {
            $this->ffmpeg_command = [$command];
        }

        return $this;
    }

    /**
     * 设置文件输出扩展名 用于转换格式等
     *
     * @param $extension
     * @return $this
     */
    public function setExtension($extension)
    {
        switch ($extension) {
            case 'mp3':
                $this->setAudioAcodec()->setAduioAr()->setAudioBit();
                break;
        }
        $this->extension = $extension;

        return $this;
    }

    /**
     * 设置音频码率
     *
     * @param int $bit 码率
     * @return $this
     */
    public function setAudioBit($bit = 64)
    {
        $this->ffmpeg_command[] = "-b:a {$bit}k";

        return $this;
    }

    /**
     * 设定音频编解码器
     *
     * @param string $acodec
     * @return $this
     */
    public function setAudioAcodec($acodec = "mp3")
    {
        $this->ffmpeg_command[] = "-acodec mp3";

        return $this;
    }

    /**
     * 设置采样率
     *
     * 采样频率一般共分为11025Hz、22050Hz、24000Hz、44100Hz、48000Hz五个等级
     * @param int $ar
     * @return $this
     */
    public function setAduioAr($ar = 24000)
    {
        $this->ffmpeg_command[] = "-ar {$ar}";

        return $this;
    }

/*-----------------------------------设置----------------------------------*/


/*-----------------------------------获取----------------------------------*/
    /**
     * 获取滤镜参数拼接的字符串
     *
     * @return string
     */
    private function getFilter()
    {
        // 滤镜
        $filter = '';

        // 判断滤镜参数是否为空
        if (!empty($this->ffmpeg_filter)) {
            // 滤镜参数字符串
            $filter_string = '';

            // 判断滤镜参数数量是否大于1 如果大于则需要加上参数名给后面的参数使用
            if (count($this->ffmpeg_filter) > 1) {
                foreach ($this->ffmpeg_filter as $k => $value) {
                    // 判断下标是否是0 如果是则初始化
                    if (empty($k)) {
                        $filter_string = $value."[content{$k}];";
                    } else {
                        // 上一个参数名
                        $prev = $k-1;

                        // 判断是否有下一个参数 如果有需要加上本参数名给下一个用
                        $next = empty($this->ffmpeg_filter[$k+1]) ? "" : "[content{$k}];" ;

                        // 拼接参数名
                        $filter_string .= "[content{$prev}]{$value}{$next}";
                    }
                }
            } else {
                // 如果只有一个则不需要拼接
                $filter_string = $this->ffmpeg_filter[0];
            }

            $filter = "-filter_complex \"{$filter_string}\" ";
        }

        return $filter;
    }

    /**
     * 获取准备执行的ffmpeg语句
     *
     * @return string
     */
    public function getCommand()
    {
        // 输入的文件
        $in_file = $this->tmp_path;

        // 输出的文件
        $out_file = $this->getFullPath();

        // 滤镜
        $filter = $this->getFilter();

        // 拼接命令字符串
        $command = "ffmpeg -y -i {$in_file} ".join(' ', $this->ffmpeg_command)." {$filter} ".$out_file;

        return $command;
    }
/*-----------------------------------获取----------------------------------*/


/*-----------------------------------执行----------------------------------*/
    /**
     * 执行FFMPEG命令【必须开通exec命令才可使用】
     *
     * @param string $command 独立执行的命令
     * @return bool 成功返回true 失败返回false
     * @throws uploadException
     */
    public function run($command = '')
    {
        if (empty($command)) {
            // 处理前检测文件路径
            $this->checkPath();

            // 获取命令
            $command = $this->getCommand();

            // 判断文件是否存在
            if ($this->check_file_exists && file_exists($this->getFullPath())) {
                $this->setError('文件已经存在');
            }
        }

        exec($command, $out_put, $status);

        // 执行失败
        if ($status != 0) {
            $this->setError('FFMPEG执行失败，请使用getCommand方法查询语句');
        }

        $this->file_size = filesize($this->getFullPath());

        $this->file_mime =  mime_content_type($this->getFullPath());

        $this->tmp_path = $this->getFullPath();

        return true;
    }
/*-----------------------------------执行----------------------------------*/


/*-----------------------------------其他----------------------------------*/
    /**
     * 检测文件类型
     *
     * @param string $type 需要检测的类型 image video radio
     * @throws uploadException
     */
    private function checkType($type)
    {
        $mime = explode('/', $this->getMime())[0];

        if ($mime == 'application/octet-stream') {
            return;
        }

        if ($mime != $type){
            $this->setError('格式不正确,不能使用相应方法');
        }
    }

/*-----------------------------------其他----------------------------------*/
}